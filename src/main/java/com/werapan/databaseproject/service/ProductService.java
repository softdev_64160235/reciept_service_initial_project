/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;


import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author toey
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();

    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>)productDao.getAll(" product_name asc");
    }

    public Product addNew(Product editedProduct) {
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        return productDao.delete(editedProduct);
    }
}
